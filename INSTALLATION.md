# ARStarKiosk

## Software Installation
Software must be installed on an Apple iPad before it may be used as a kiosk.

1. Using Safari on the iPad, visit <https://static.ark.org/arstarkiosk/index.html> and tap the installation link.
2. Tap "Install" when prompted that static.ark.org would like to install "ARStarKiosk".

<img src="Documentation/installation/kiosk_installation_prompt.png" />

## Configuration
Once the application is installed on the iPad, you need to verify/update the configuration settings.

1. Open the iPad Settings
<img src="Documentation/installation/ios_settings_icon.png" />
2. Choose "ARStarKiosk" from the list on the left and modify the "Home Page" url as needed. The url should have a kiosk_id parameter indicating the identifier for the kiosk device this is being installed on. Kiosk Ids are defined by DFA.
    - <https://www.ark.org/arstar/kiosk.php?kiosk_id=9971K01> (Production)
    - <https://dev.ark.org/dfa_tagrenewal/kiosk.php?kiosk_id=9971K01> (Testing)

<img src="Documentation/installation/kiosk_settings.png" />
3. Choose the "General" item from the list on the left and verify "Multitasking Gestures" is toggled off as shown below.
<img src="Documentation/installation/kiosk_settings_gestures.png" />

## Launching the Kiosk Application
Tap the ARStarKiosk icon on the iPad home screen to launch it. Verify the correct kiosk id and environment (production or testing) is displayed at the bottom right of the screen.

<img src="Documentation/installation/kiosk_id.png" />
