# ARStarKiosk

This is a simple iOS native wrapper application for running the AR STAR application on an iPad-based Kiosk.

It provides credit card swipe integration to simplify the checkout process.


## Directory Structure
- /ARStarKiosk-Installer/
    - Files needed for installing the kiosk application on iOS.
- /ARStarKiosk-Xcode/
    - The xcode project for the native wrapper. Based on sample SDK files from <http://www.lilitab.com/pages/downloads>.

## Distribution

## Installation
See INSTALLATION.md for installation instructions.
