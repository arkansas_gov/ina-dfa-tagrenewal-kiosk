# ARStarKiosk

## Software Installation
Software must be installed on an Apple iPad before it may be used as a kiosk.

1. Using Safari on the iPad, visit <https://static.ark.org/arstarkiosk/index.html> and tap the installation link.
2. Tap "Install" when prompted that static.ark.org would like to install "ARStarKiosk".

<img src="Documentation/installation/kiosk_installation_prompt.png" />

## Launching the Kiosk Application
Tap the ARStarKiosk icon on the iPad home screen to launch it. Verify the correct kiosk id and environment (production or testing) is displayed at the bottom right of the screen.

<img src="Documentation/installation/kiosk_id.png" />
