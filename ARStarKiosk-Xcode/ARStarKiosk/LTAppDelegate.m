//
//  LTAppDelegate.m
//  LilitabWebDemo
//
//  Created on 4/20/12.
//  Copyright (c) 2012 Lilitab, LLC. All rights reserved.
//

#import "LTAppDelegate.h"
#import <sys/utsname.h>


@interface LTAppDelegate ()

- (void)initializeDefaults;

@end

@implementation LTAppDelegate

@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [self initializeDefaults];
    
    return YES;
}

NSString* deviceName()
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)initializeDefaults {
    NSLog(@"initializeDefaults");
	// Set the application defaults. Before you can use defaults in the
    // Settings bundle, you have to set the default values in code.
    //
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //
    //  The home page is where you want the app to go when it is launched. In general,
    //  you want to set that here in code and in the settings app on the iPad springboard.
    //
    if ([defaults objectForKey:kENVIRONMENT_KEY] == nil) {
        [defaults	setBool:YES forKey:kENVIRONMENT_KEY];
    }
    if ([defaults objectForKey:kKIOSK_ID_KEY] == nil) {
        [defaults	setObject:kKIOSK_ID_DEFAULT forKey:kKIOSK_ID_KEY];
    }
    
    
    // JavaScript Function Names
    
    if ([defaults objectForKey:kCALL_ON_SWIPE_KEY] == nil) {
		[defaults	setObject: kCALL_ON_SWIPE_DEFAULT forKey:kCALL_ON_SWIPE_KEY];
	}
    if ([defaults objectForKey:kCALL_ON_CONNECT_KEY] == nil) {
		[defaults	setObject: kCALL_ON_CONNECT_DEFAULT forKey:kCALL_ON_CONNECT_KEY];
	}
    if ([defaults objectForKey:kCALL_ON_DISCONNECT_KEY] == nil) {
		[defaults	setObject: kCALL_ON_DISCONNECT_DEFAULT forKey:kCALL_ON_DISCONNECT_KEY];
	}
    if ([defaults objectForKey:kCALL_ON_PARAMETER_KEY] == nil) {
		[defaults	setObject: kCALL_ON_PARAMETER_DEFAULT forKey:kCALL_ON_PARAMETER_KEY];
	}   


   
	if ([defaults synchronize]) {
        DLog(@"Defaults Synchronized");
    } else {
        DLog(@"Defaults NOT Synchronized");
    }
    
}

@end
