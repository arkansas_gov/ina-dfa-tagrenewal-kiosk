//
//  LTAppDelegate.h
//  LilitabWebDemo
//
//  Created on 4/20/12.
//  Copyright (c) 2012 Lilitab, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTWebDemoConstants.h"

@interface LTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
