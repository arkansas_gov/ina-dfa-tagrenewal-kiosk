//
//  LTCustomLogger.m
//  ARStarKiosk
//
//  Created by Bob Sanders on 8/17/19.
//  Copyright © 2019 Basically Bits, LLC. All rights reserved.
//

#import "LTCustomLogger.h"
@import MobileCoreServices;
@implementation LTCustomLogger



-(id)init {
    if ( self = [super init] ) {
        //[[NSFileManager defaultManager] createFileAtPath:@"Your/Path" contents:nil attributes:nil];
        _fileManager = [NSFileManager defaultManager];
        _dateFormatter=[[NSDateFormatter alloc] init];
        _lastTime = [[NSDate date] timeIntervalSince1970];
        
        NSLog(@"LTCustomLogger writing to %@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    }
    return self;
}

-(double)getElapsed{
    double now = [[NSDate date] timeIntervalSince1970];
    double elapsed = now - _lastTime;
    //NSLog(@"%f Elapsed", elapsed);
    _lastTime = [[NSDate date] timeIntervalSince1970];
    return elapsed;
}

- (NSString *) getDateFileName
{
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    [_dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *fileName = [NSString stringWithFormat:@"%@/%@",
                          documentsDirectory,
                          [_dateFormatter stringFromDate:[NSDate date]]];
    
    return fileName;
}



- (void)log:(NSString *)message
{
    [_dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *logline = [NSString stringWithFormat:@"%@ %@ %f %@\n",
                         [_dateFormatter stringFromDate:[NSDate date]],
                         _identifier,
                         [self getElapsed],
                         message];
    
    NSString *fileName = [self getDateFileName];
    if(![_fileManager fileExistsAtPath:fileName])
    {
        [logline writeToFile:fileName atomically:NO encoding:NSUTF8StringEncoding error:nil];
    }
    else
    {
        NSFileHandle *myHandle = [NSFileHandle fileHandleForWritingAtPath:fileName];
        [myHandle seekToEndOfFile];
        [myHandle writeData:[logline dataUsingEncoding:NSUTF8StringEncoding]];
    }
   
}

- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters
                             paths:(NSArray *)paths
                         fieldName:(NSString *)fieldName {
    NSMutableData *httpBody = [NSMutableData data];
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    for (NSString *path in paths) {
        NSString *filename  = [path lastPathComponent];
        NSData   *data      = [NSData dataWithContentsOfFile:path];
        NSString *mimetype  = @"text/plain";//[self mimeTypeForPath:path];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldName, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

// not used - got an error and hard coded the mime type
- (NSString *)mimeTypeForPath:(NSString *)path {
    // get a mime type for an extension using MobileCoreServices.framework
    
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    assert(mimetype != NULL);
    
    CFRelease(UTI);
    
    return mimetype;
}

- (NSString *)generateBoundaryString {
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}

- (void) uploadLogFile{
    [self log:@"Uploading log file..."];
    
    NSDictionary *params = @{@"login"     : @"kiosk",
                             @"password"  : @"n9KFdzLKqNoLYFTYqjjBvqx",
                             @"identifier": _identifier
                             
                             };
    NSString *path = [self getDateFileName];
    
    NSString *boundary = [self generateBoundaryString];
    
    // configure the request
    NSString *url = @"http://moody.nic-ar.io/kiosk/logs/logger.php";
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    
    // set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create body
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:params paths:@[path] fieldName:@"file"];
    
    NSURLSession *session = [NSURLSession sharedSession];  // use sharedSession or create your own
    
    NSURLSessionTask *task = [session uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            //NSLog(@"Log upload error = %@", error);
            [self log:[NSString stringWithFormat:@"Upload error = %@", error]];
            return;
        }else{
            NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            //NSLog(@"Log upload success = %@", result);
            [self log:[NSString stringWithFormat:@"Upload complete, %lu bytes, error = %@", (unsigned long)httpBody.length, result]];
        }
        
    }];
    [task resume];
    
}


@end
    
