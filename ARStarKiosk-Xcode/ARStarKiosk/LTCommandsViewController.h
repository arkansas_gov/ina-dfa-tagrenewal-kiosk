//
//  LTCommandsViewController.h
//  LilitabWebDemo
//
//  Created on 4/20/12.
//  Copyright (c) 2012 Lilitab, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LTDeviceCommandDelegate <NSObject>

- (void)commandWasSelected:(NSIndexPath *)commandIndexPath;

@end

@interface LTCommandsViewController : UITableViewController

@property (weak, nonatomic) id <LTDeviceCommandDelegate> delegate;

@end
