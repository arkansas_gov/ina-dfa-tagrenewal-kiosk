//
//  LTLightningViewController.m
//  ARStarKiosk
//
//  Created by Maciej Kita on 3/10/17.
//  Copyright © 2017 Basically Bits, LLC. All rights reserved.
//

#import "LTLightningViewController.h"
#import "LilitabSDK/LilitabSDK.h"
#import "Reachability.h"
#import "LTCustomLogger.h"

@interface LTLightningViewController () {
    Reachability *networkReachability;
    NSDate *outDate;
    LTCustomLogger *logger;
}
@property (weak, nonatomic) IBOutlet UIView *unreachable;

@property int ticksSinceHalted;
@property int ticksUntilReload;
@property int secondsBetweenTicks;
@property BOOL isHalted;
@property NSString *kioskId;

@end

@implementation LTLightningViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    NSUserDefaults *defaults    =    [NSUserDefaults    standardUserDefaults];
    BOOL prod = [defaults boolForKey:kENVIRONMENT_KEY];
    _kioskId            =    [defaults    stringForKey:kKIOSK_ID_KEY];
    NSString *homePageNameString            =    [NSString stringWithFormat:prod?kHOMEPAGE_ADDRESS:kHOMEPAGE_ADDRESS_DEV, _kioskId];
    NSURL    *url = [NSURL URLWithString:(NSString *)homePageNameString];
    NSString* host = [url host];
    
    // init the logger
    logger = [[LTCustomLogger alloc] init];
    logger.identifier = [defaults    stringForKey:kKIOSK_ID_KEY];
    [logger log:@"viewDidLoad"];
    
    // init the reachability sensor
    networkReachability = host==nil?[Reachability reachabilityForInternetConnection]:[Reachability reachabilityWithHostName:host];
    [networkReachability startNotifier];

    // init the webview
    _wkWebView = [[WKWebView alloc] initWithFrame:_webViewContainer.frame];
    _wkWebView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    [_webViewContainer addSubview:_wkWebView];
    _wkWebView.navigationDelegate = self;

    // load the home page
    [self gotoHomePage];
    
    // initialize the timer
    self.secondsBetweenTicks = 10;
    self.ticksUntilReload = 6;
    self.ticksSinceHalted = 0;
    self.isHalted = NO;
    
    // start the timer for watching for page loading issues
    [NSTimer scheduledTimerWithTimeInterval: self.secondsBetweenTicks
                                     target: self
                                   selector:@selector(onTick:)
                                   userInfo: nil repeats:YES];

    // how often do we want to upload log files?
    [NSTimer scheduledTimerWithTimeInterval: 60
                                     target: self
                                   selector:@selector(logfileUploadTimer:)
                                   userInfo: nil repeats:YES];
    
    [logger log:@"Startup completed"];
    
    // get our GPS location
    /*
     https://stackoverflow.com/questions/4152003/how-can-i-get-current-location-from-user-in-ios
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];
     */
    
}


#pragma mark - utility methods

- (BOOL) checkKioskIdIsSet{
    
    if([self.kioskId isEqualToString:@"9999K01"]){
        // uh oh! We're running with the default!
        [logger log:[NSString stringWithFormat:@"checkKioskIdIsSet failed with %@", self.kioskId]];
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Kiosk ID Error"
                                                                       message:@"The kiosk id is not set. Please configure it in settings."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  [self gotoHomePage];
                                                              }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return false;
    }
    
    return true;
}

-(void)logfileUploadTimer:(NSTimer *)timer {
    [logger uploadLogFile];
}

-(void) setLoadStatus:(NSString *)status{
    _statusLabel.text = status;
}

-(void) startSpinner{
    _spinner.hidden = NO;
    [self setLoadStatus:@"Please wait..."];
    _statusLabel.hidden = NO;
    [_spinner startAnimating];
    
    // disable user interaction inside the webview
    for (UIView *subview in _wkWebView.scrollView.subviews)
    {
        subview.userInteractionEnabled = NO;
    }
}

-(void) stopSpinner{
    _spinner.hidden = YES;
    _statusLabel.hidden = YES;
    [_spinner stopAnimating];
    
    // enable user interaction inside the webview
    for (UIView *subview in _wkWebView.scrollView.subviews)
    {
        subview.userInteractionEnabled = YES;
    }
}

- (IBAction)longTapEvent:(id)sender {
    [logger log:@"Long tap detected"];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Reload the page?"
                                          message:@"If you are having trouble with this kiosk, tap 'OK' to try reloading the page."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    // Cancel Button
    UIAlertAction *actionCancel = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                                       [logger log:@"Long tap canceled"];
                                   }];
    
    [actionCancel setValue:[UIColor redColor] forKey:@"titleTextColor"];
    [alertController addAction:actionCancel];

    // ok Button
    UIAlertAction *actionOk = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                   [logger log:@"Long tap ok - reloading page"];
                                   [self gotoHomePage];
                               }];
    [alertController addAction:actionOk];
    alertController.preferredAction = actionOk;

    
    [self presentViewController:alertController animated:YES completion:nil];
    
}



// Home Page Convienence Method
-(void) gotoHomePage {
    
    [logger log:[NSString stringWithFormat:@"gotoHomePage"]];
    NSUserDefaults *defaults    =    [NSUserDefaults    standardUserDefaults];
    BOOL prod = [defaults boolForKey:kENVIRONMENT_KEY];
    NSString *kioskId            =    [defaults    stringForKey:kKIOSK_ID_KEY];
    NSString *homePageNameString            =    [NSString stringWithFormat:prod?kHOMEPAGE_ADDRESS:kHOMEPAGE_ADDRESS_DEV, kioskId];
    NSURL    *url = [NSURL URLWithString:(NSString *)homePageNameString];
    NSMutableURLRequest    *request = [NSMutableURLRequest    requestWithURL:url];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];  // prevent using cached page
    self.ticksSinceHalted=0;

    [_wkWebView loadRequest:request];
}

// this method gets called with every timer tick
-(void)onTick:(NSTimer *)timer {
    
    if(self.isHalted==YES){
        self.ticksSinceHalted++;
        [logger log:[NSString stringWithFormat:@"Dialog up for %d seconds", self.ticksSinceHalted]];
        if(self.ticksSinceHalted > self.ticksUntilReload){
            NSLog(@"Dialog up for %d seconds, reloading", self.ticksSinceHalted);
            [logger log:[NSString stringWithFormat:@"Dialog up for %d seconds, reloading", self.ticksSinceHalted]];
            
            // make the reload button go away
            [self dismissViewControllerAnimated:YES completion:nil];
            
            // reset variables and reload
            self.ticksSinceHalted=0;
            self.isHalted = NO;
            [self gotoHomePage];
            return;
        }
    }
    NSLog(@"Tick [Halted? %@ For %ds] Loading? %@",
          self.isHalted ? @"Yes" : @"No",
          self.ticksSinceHalted,
          _wkWebView.isLoading ? @"Yes" : @"No"
          );
}

#pragma mark - add event observers

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkStatusChanges:) name:kReachabilityChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lilitabConnected:) name:LilitabSDK_DidConnectNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lilitabDisconnected:) name:LilitabSDK_DidDisconnectNotification object:nil];
    
    [[LilitabSDK singleton] scanForConnectedAccessories];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)networkStatusChanges:(NSNotification *)notification {
    
    Reachability* r = [notification object];
    if ([r currentReachabilityStatus] != NotReachable) {
        NSLog(@"Reachable");
        [logger log:[NSString stringWithFormat:@"Reachable"]];
        
    } else {
        NSLog(@"NOT Reachable");
        [logger log:[NSString stringWithFormat:@"Not Reachable"]];
    }
}
#pragma mark - Lilitab event handling

- (void) lilitabConnected:(NSNotification*) notification {
    NSLog(@"reader was connected %ld", (long)[LilitabSDK singleton].accessoryType);
    
    [logger log:[NSString stringWithFormat:@"reader was connected %ld", (long)[LilitabSDK singleton].accessoryType]];
    
    self.readerConnectedImageView.image = [UIImage	imageNamed:@"yesReader.png"];
    
    [LilitabSDK singleton].swipeBlock = ^(NSDictionary* swipeData) {
        // Card is swiped
        NSString* cardswipe = [NSString stringWithFormat:@"%%%@?;%@?", swipeData[@"Track1"], swipeData[@"Track2"]];
        NSUserDefaults *defaults    =    [NSUserDefaults    standardUserDefaults];
        
        // Build the JavaScript string that will be run on the web page and run it
        [logger log:[NSString stringWithFormat:@"Swipe detected"]];
        NSString *js = [NSString stringWithFormat:@"var trackData = \"%@\"; %@(trackData);",cardswipe, [defaults objectForKey:kCALL_ON_SWIPE_KEY]];
        
        // commented this out.. don't want it recording track data
        //DLog(@"js = %@",js);
        
        [_wkWebView evaluateJavaScript:js completionHandler:^(id result, NSError *error) {
           [logger log:[NSString stringWithFormat:@"Swipe complete"]];
        }];
        
    };
    [LilitabSDK singleton].swipeTimeout = 0;
    [LilitabSDK singleton].enableSwipe = YES;
    [LilitabSDK singleton].allowMultipleSwipes = YES;
}

- (void) lilitabDisconnected:(NSNotification*) notification {
    self.readerConnectedImageView.image = [UIImage	imageNamed:@"noReader.png"];
}



#pragma mark - WebView Delegate methods

- (void) handleNavigationError:(WKNavigation *)navigation withError:(NSError *)error{
    
    NSLog(@"handleNavigationError %@", error);
    [logger log:[NSString stringWithFormat:@"handleNavigationError %@ to %@", error, [_wkWebView.URL absoluteURL]]];
    
    // let's not fail with a -999. That's when someone taps a link while the page is loading.
    if(error.code == -999){
        return;
    }
    
    BOOL haltBrowser = false;
    NSString *errorString = @"Unknown error";
    
    haltBrowser = true;
    errorString = error.domain;
    
    if(haltBrowser){
        self.isHalted = YES;
        [self setLoadStatus:errorString];
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Page load error"
                                                                       message:@"Something went wrong with the last request. Please try again."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Reload" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  self.isHalted = NO;
                                                                  [self gotoHomePage];
                                                              }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    NSLog(@"didFailNavigation for %@", [webView.URL absoluteURL]);
    [self handleNavigationError:navigation withError:error];
    NSLog(@" after handleNavigationError");
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    NSLog(@"didFailNavigation for %@", [webView.URL absoluteURL]);
    [self handleNavigationError:navigation withError:error];
    NSLog(@" after handleNavigationError");
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    NSLog(@"didFinishNavigation to %@", [webView.URL absoluteURL]);
    [logger log:[NSString stringWithFormat:@"didFinishNavigation to %@", [webView.URL absoluteURL]]];
    [self stopSpinner];
    [self checkKioskIdIsSet];
}

- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView{
    NSLog(@"webViewWebContentProcessDidTerminate");
    [logger log:[NSString stringWithFormat:@"webViewWebContentProcessDidTerminate at %@", [webView.URL absoluteURL]]];
}

- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation{
    NSLog(@"didCommitNavigation to %@", [webView.URL absoluteURL]);
    [logger log:[NSString stringWithFormat:@"didCommitNavigation to %@", [webView.URL absoluteURL]]];
    [self startSpinner];
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    NSLog(@"didStartProvisionalNavigation at %@", [webView.URL absoluteURL]);
    [logger log:[NSString stringWithFormat:@"didStartProvisionalNavigation to %@", [webView.URL absoluteURL]]];
    [self startSpinner];
    
}


@end
