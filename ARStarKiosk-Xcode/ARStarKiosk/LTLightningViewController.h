//
//  LTLightningViewController.h
//  ARStarKiosk
//
//  Created by Maciej Kita on 3/10/17.
//  Copyright © 2017 Basically Bits, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTWebDemoConstants.h"
#import <WebKit/WebKit.h>


@interface LTLightningViewController : UIViewController<WKNavigationDelegate>

@property (weak, nonatomic) IBOutlet UIView *webViewContainer;
@property(strong,nonatomic) WKWebView *wkWebView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightBarButton;

@property (strong, nonatomic) IBOutlet UIImageView *readerConnectedImageView;

@end
