//
//  main.m
//  LilitabWebDemo
//
//  Created on 4/20/12.
//  Copyright (c) 2012 Lilitab, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LTAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LTAppDelegate class]));
    }
}
