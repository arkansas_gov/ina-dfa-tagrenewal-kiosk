//
//  LTWebDemoConstants.h
//  LilitabWebDemo
//
//  Created on 4/20/12.
//  Copyright (c) 2012 Lilitab, LLC. All rights reserved.
//

#ifndef LilitabWebDemo_LTWebDemoConstants_h
#define LilitabWebDemo_LTWebDemoConstants_h

#ifdef DEBUG
#    define DLog(...) NSLog(__VA_ARGS__)
#else
#    define DLog(...) NSLog(__VA_ARGS__) 
//DLog(...) /* */
#endif

#define     kTRACK1_DATA                        @"trackOne"
#define     kTRACK2_DATA                        @"trackTwo"
#define     kALL_READER_DATA                    @"allData"
#define     kMAX_TRACK1_AND_2_LENGTH    125

// Web Address
#define     kENVIRONMENT_KEY            @"environment"
#define     kKIOSK_ID_KEY               @"kiosk_id"
#define     kKIOSK_ID_DEFAULT           @"9999K01"
#define     kHOMEPAGE_ADDRESS_DEV       @"https://dev.ark.org/dfa_tagrenewal/kiosk.php?kiosk_id=%@"
#define     kHOMEPAGE_ADDRESS           @"https://www.ark.org/arstar/kiosk.php?kiosk_id=%@"

// JavaScript Functions
#define kCALL_ON_SWIPE_KEY              @"call_on_swipe"
#define kCALL_ON_SWIPE_DEFAULT          @"handleLiliSwipe"
#define kCALL_ON_CONNECT_KEY            @"call_on_connect"
#define kCALL_ON_CONNECT_DEFAULT        @"readerConnected"
#define kCALL_ON_DISCONNECT_KEY         @"call_on_disconnect"
#define kCALL_ON_DISCONNECT_DEFAULT     @"readerDisonnected"
#define kCALL_ON_PARAMETER_KEY         @"call_on_parameter"
#define kCALL_ON_PARAMETER_DEFAULT     @"parseParameterData"


#define     kPASS_ENCRYPTED_READER_IN_USE_KEY @"encrypted_reader"
#define     kPASS_ENCRYPTED_READER_IN_USE_DEFAULT   NO
#define     kPASS_RAW_DATA_KEY          @"pass_raw_data"
#define     kPASS_RAW_DATA_FIELD_ID     @"trk_data_field"
#define     kPASS_RAW_DATA_FIELD_DEFAULT @"trkAdata"



#endif
