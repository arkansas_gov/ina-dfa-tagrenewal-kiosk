//
//  LTCustomLogger.h
//  ARStarKiosk
//
//  Created by Bob Sanders on 8/17/19.
//  Copyright © 2019 Basically Bits, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LTCustomLogger : NSObject

@property NSFileManager *fileManager;
@property NSDateFormatter *dateFormatter;
@property (nonatomic) NSString *identifier;
@property double lastTime;

- (void)log:(NSString *)message;
- (void) uploadLogFile;
-(double)getElapsed;

@end

NS_ASSUME_NONNULL_END
